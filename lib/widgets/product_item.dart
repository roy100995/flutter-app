import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/models/app_state.dart';
import 'package:flutter_ecommerce/models/product.dart';
import 'package:flutter_ecommerce/pages/product_detail_page.dart';
import 'package:flutter_ecommerce/redux/actions.dart';
import 'package:flutter_redux/flutter_redux.dart';

class ProductItem extends StatelessWidget {
  final Product item;
  ProductItem({this.item});

  bool _isInCart(AppState state, String id) {
    final List<Product> cartProducts = state.cartProducts;
    return cartProducts.indexWhere((cartProduct) => cartProduct.id == id) > -1;
  }

  @override
  Widget build(BuildContext context) {
    final String url = item.picture['url'];
    final String pictureUrl = 'http://10.0.2.2:1337$url';
    return InkWell(
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) {
            return ProductDetailPage(item: item);
          },
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.only(bottom: 10.0),
        child: GridTile(
          child: Hero(
            tag: item.id,
            child: Image.network(
              pictureUrl,
              fit: BoxFit.cover,
            ),
          ),
          footer: GridTileBar(
            backgroundColor: Color(0xBB000000),
            trailing: StoreConnector<AppState, AppState>(
              converter: (store) => store.state,
              builder: (_, state) {
                return state.user != null
                    ? IconButton(
                        icon: _isInCart(state, item.id)
                            ? Icon(Icons.remove_circle_outline)
                            : Icon(Icons.add_shopping_cart),
                        color: _isInCart(state, item.id)
                            ? Colors.white70
                            : Colors.white,
                        onPressed: () => StoreProvider.of<AppState>(context)
                            .dispatch(toggleCartProductAction(item)))
                    : Text('');
              },
            ),
            title: FittedBox(
              fit: BoxFit.scaleDown,
              alignment: Alignment.centerLeft,
              child: Text(
                item.name,
                style: TextStyle(
                  fontSize: 20.0,
                ),
              ),
            ),
            subtitle: Text(
              'S/. ${item.price}',
              style: TextStyle(
                fontSize: 16.0,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
