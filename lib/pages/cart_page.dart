import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/models/app_state.dart';
import 'package:flutter_ecommerce/widgets/product_item.dart';
import 'package:flutter_redux/flutter_redux.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  Widget _cartTab() {
    final bool portrait =
        MediaQuery.of(context).orientation == Orientation.portrait;
    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (_, state) {
        return Column(
          children: [
            Expanded(
              child: SafeArea(
                top: false,
                bottom: false,
                child: GridView.builder(
                  itemCount: state.cartProducts.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: portrait ? 1 : 2,
                      mainAxisSpacing: portrait ? 0.0 : 20.0,
                      crossAxisSpacing: 10.0,
                      childAspectRatio: portrait ? 1.0 : 1.2),
                  padding: EdgeInsets.all(30.0),
                  itemBuilder: (context, i) =>
                      ProductItem(item: state.cartProducts[i]),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget _cardsTab() {
    return Text('Cards');
  }

  Widget _receiptTab() {
    return Text('Receipt');
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Cart Page'),
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.shopping_cart)),
              Tab(icon: Icon(Icons.credit_card)),
              Tab(icon: Icon(Icons.receipt)),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            _cartTab(),
            _cardsTab(),
            _receiptTab(),
          ],
        ),
      ),
    );
  }
}
