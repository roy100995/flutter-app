import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/models/app_state.dart';
import 'package:flutter_ecommerce/models/product.dart';
import 'package:flutter_ecommerce/redux/actions.dart';
import 'package:flutter_redux/flutter_redux.dart';

class ProductDetailPage extends StatelessWidget {
  final Product item;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  ProductDetailPage({this.item});

  bool _isInCart(AppState state, String id) {
    final List<Product> cartProducts = state.cartProducts;
    return cartProducts.indexWhere((cartProduct) => cartProduct.id == id) > -1;
  }

  @override
  Widget build(BuildContext context) {
    final String url = item.picture['url'];
    final String pictureUrl = 'http://10.0.2.2:1337$url';
    final bool portrait =
        MediaQuery.of(context).orientation == Orientation.portrait;
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(item.name),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: 10.0),
                child: Hero(
                  tag: item.id,
                  child: Image.network(
                    pictureUrl,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Text(
                item.name,
                style: Theme.of(context).textTheme.headline3,
              ),
              Text(
                'S/. ${item.price}',
                style: Theme.of(context).textTheme.headline4,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 32.0),
                child: StoreConnector<AppState, AppState>(
                  converter: (store) => store.state,
                  builder: (_, state) {
                    return state.user != null
                        ? IconButton(
                            icon: _isInCart(state, item.id)
                                ? Icon(Icons.remove_circle_outline)
                                : Icon(Icons.add_shopping_cart),
                            color: _isInCart(state, item.id)
                                ? Colors.white70
                                : Colors.white,
                            onPressed: () {
                              StoreProvider.of<AppState>(context)
                                  .dispatch(toggleCartProductAction(item));
                              final snackBar = SnackBar(
                                backgroundColor: Colors.white,
                                duration: Duration(seconds: 2),
                                content: Text(
                                  'Cart Updated',
                                  style: TextStyle(color: Colors.green),
                                ),
                              );
                              _scaffoldKey.currentState.showSnackBar(snackBar);
                            })
                        : Text('');
                  },
                ),
              ),
              Padding(
                child: Text(
                  item.description,
                  style: Theme.of(context).textTheme.bodyText1,
                  textAlign: TextAlign.justify,
                ),
                padding: portrait
                    ? EdgeInsets.only(left: 32.0, right: 32.0, bottom: 32.0)
                    : EdgeInsets.only(left: 80.0, right: 80.0, bottom: 32.0),
              )
            ],
          ),
        ),
      ),
    );
  }
}
