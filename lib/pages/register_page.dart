import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterPage extends StatefulWidget {
  @override
  RegisterPageState createState() => RegisterPageState();
}

class RegisterPageState extends State<RegisterPage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  bool _isSubmitting, _obscureText = true;
  String _username, _email, _password;

  //========= Widgets ==========//

  Widget _title() {
    return Text('Registro',
        style: Theme.of(context)
            .textTheme
            .headline1
            .copyWith(color: Colors.white));
  }

  Widget _userInput() {
    return Padding(
        padding: EdgeInsets.only(top: 30.0),
        child: TextFormField(
          onSaved: (val) => _username = val,
          validator: (val) =>
              val.length < 6 ? 'Nombre de usuario demasiado corto' : null,
          decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Usuario',
              hintText: 'Mínimo 6 caracteres',
              icon: Icon(
                Icons.account_circle,
                color: Theme.of(context).primaryColor,
              )),
        ));
  }

  Widget _emailInput() {
    return Padding(
        padding: EdgeInsets.only(top: 20.0),
        child: TextFormField(
          onSaved: (val) => _email = val,
          validator: (val) =>
              !val.contains('@') ? 'Ingrese un email válido' : null,
          decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Email',
              hintText: 'Ingresa un email válido',
              icon: Icon(
                Icons.mail,
                color: Theme.of(context).primaryColor,
              )),
        ));
  }

  Widget _passwordInput() {
    return Padding(
        padding: EdgeInsets.only(top: 20.0),
        child: TextFormField(
          onSaved: (val) => _password = val,
          obscureText: _obscureText,
          validator: (val) =>
              val.length < 6 ? 'Contraseña demasiado corta' : null,
          decoration: InputDecoration(
              suffixIcon: GestureDetector(
                  onTap: () {
                    setState(() => _obscureText = !_obscureText);
                  },
                  child: Icon(
                      _obscureText ? Icons.visibility : Icons.visibility_off)),
              border: OutlineInputBorder(),
              labelText: 'Contraseña',
              hintText: 'Mínimo 6 caracteres',
              icon: Icon(
                Icons.lock,
                color: Theme.of(context).primaryColor,
              )),
        ));
  }

  Widget _formActions() {
    return Padding(
      padding: EdgeInsets.only(top: 20.0),
      child: Column(
        children: [
          _isSubmitting == true
              ? CircularProgressIndicator(
                  valueColor:
                      AlwaysStoppedAnimation(Theme.of(context).primaryColor),
                )
              : RaisedButton(
                  child: Text(
                    'Registrarse',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  elevation: 4.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  color: Theme.of(context).primaryColor,
                  onPressed: _submit,
                ),
          FlatButton(
              child: Text('¿Ya tienes una cuenta? Ingresa aquí'),
              onPressed: () =>
                  Navigator.pushReplacementNamed(context, '/login'))
        ],
      ),
    );
  }

  //========= Functions ==========//

  void _submit() {
    final form = _formKey.currentState;

    if (form.validate()) {
      form.save();
      _registerUser();
    }
  }

  void _registerUser() async {
    setState(() => _isSubmitting = true);

    http.Response response = await http.post(
        'http://10.0.2.2:1337/auth/local/register',
        body: {"username": _username, "email": _email, "password": _password});
    final responseData = json.decode(response.body);
    print(responseData);
    if (response.statusCode == 200) {
      setState(() => _isSubmitting = false);
      _storeUserData(responseData);
      _showSnack();
      _redirectUser();
    } else {
      setState(() => _isSubmitting = false);
      final String errorMsg =
          responseData['message'][0]['messages'][0]['message'];

      _showErrorSnack(errorMsg);
    }
  }

  void _storeUserData(responseData) async {
    final prefs = await SharedPreferences.getInstance();
    Map<String, dynamic> user = responseData['user'];
    user.putIfAbsent('jwt', () => responseData['jwt']);
    prefs.setString('user', json.encode(user));
  }

  void _showSnack() {
    final snackbar = SnackBar(
      content: Text('El usuario $_username ha sido registrado!',
          style: TextStyle(color: Colors.white)),
      backgroundColor: Colors.transparent,
    );

    _scaffoldKey.currentState.showSnackBar(snackbar);
    _formKey.currentState.reset();
  }

  void _showErrorSnack(String errorMsg) {
    final snackbar = SnackBar(
      content: Text(errorMsg, style: TextStyle(color: Colors.red)),
      backgroundColor: Colors.transparent,
    );

    _scaffoldKey.currentState.showSnackBar(snackbar);

    throw Exception('Error al registrarse: $errorMsg');
  }

  void _redirectUser() {
    Future.delayed(Duration(seconds: 2), () {
      Navigator.pushReplacementNamed(context, '/');
    });
  }

  //========= Body draw ==========//
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text('Registro'),
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 30.0),
          child: Center(
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    _title(),
                    _userInput(),
                    _emailInput(),
                    _passwordInput(),
                    _formActions()
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
