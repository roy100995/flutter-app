import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/models/app_state.dart';
import 'package:flutter_ecommerce/redux/actions.dart';
import 'package:flutter_ecommerce/widgets/product_item.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:badges/badges.dart';

final gradientBackground = BoxDecoration(color: Colors.white);

class ProductsPage extends StatefulWidget {
  final void Function() onInit;
  ProductsPage({this.onInit});

  @override
  ProductsPageState createState() => ProductsPageState();
}

class ProductsPageState extends State<ProductsPage> {
  void initState() {
    super.initState();
    widget.onInit();
  }

  final _appBar = PreferredSize(
      preferredSize: Size.fromHeight(60.0),
      child: StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          return AppBar(
            centerTitle: true,
            title: SizedBox(
                child: state.user != null
                    ? Text(state.user.username)
                    : FlatButton(
                        child: Text(
                          'Registro',
                          style: Theme.of(context).textTheme.headline5,
                        ),
                        onPressed: () =>
                            Navigator.pushNamed(context, '/register'),
                      )),
            leading: state.user != null
                ? Badge(
                    badgeContent: Text(state.cartProducts.length.toString()),
                    position: BadgePosition.topRight(top: 0, right: 0),
                    child: IconButton(
                      icon: Icon(Icons.local_grocery_store),
                      onPressed: () => Navigator.pushNamed(context, '/cart'),
                    ),
                  )
                : Text(''),
            actions: [
              Padding(
                  padding: EdgeInsets.only(right: 12.0),
                  child: StoreConnector<AppState, VoidCallback>(
                    converter: (store) {
                      return () => store.dispatch(logoutUserAction);
                    },
                    builder: (_, callBack) {
                      return state.user != null
                          ? IconButton(
                              icon: Icon(Icons.exit_to_app),
                              onPressed: callBack)
                          : Text('');
                    },
                  ))
            ],
          );
        },
      ));

  @override
  Widget build(BuildContext context) {
    final bool portrait =
        MediaQuery.of(context).orientation == Orientation.portrait;
    return Scaffold(
      appBar: _appBar,
      body: Container(
        decoration: gradientBackground,
        child: StoreConnector<AppState, AppState>(
          converter: (store) => store.state,
          builder: (_, state) {
            return Column(
              children: [
                Expanded(
                  child: SafeArea(
                    top: false,
                    bottom: false,
                    child: GridView.builder(
                      itemCount: state.products.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: portrait ? 1 : 2,
                          mainAxisSpacing: portrait ? 0.0 : 20.0,
                          crossAxisSpacing: 10.0,
                          childAspectRatio: portrait ? 1.0 : 1.2),
                      itemBuilder: (context, i) =>
                          ProductItem(item: state.products[i]),
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
