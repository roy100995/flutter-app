import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  bool _isSubmitting, _obscureText = true;
  String _email, _password;

  Widget _title() {
    return Text('Inicio de sesión',
        style: Theme.of(context)
            .textTheme
            .headline1
            .copyWith(color: Colors.white));
  }

  Widget _emailInput() {
    return Padding(
        padding: EdgeInsets.only(top: 30.0),
        child: TextFormField(
          onSaved: (val) => _email = val,
          validator: (val) =>
              !val.contains('@') ? 'Ingresa un email válido' : null,
          decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Email',
              hintText: 'Ingresa un email válido',
              icon: Icon(
                Icons.mail,
                color: Theme.of(context).primaryColor,
              )),
        ));
  }

  Widget _passwordInput() {
    return Padding(
        padding: EdgeInsets.only(top: 20.0),
        child: TextFormField(
          onSaved: (val) => _password = val,
          obscureText: _obscureText,
          validator: (val) =>
              val.length < 6 ? 'La contraseña es incorrecta' : null,
          decoration: InputDecoration(
              suffixIcon: GestureDetector(
                  onTap: () {
                    setState(() => _obscureText = !_obscureText);
                  },
                  child: Icon(
                      _obscureText ? Icons.visibility : Icons.visibility_off)),
              border: OutlineInputBorder(),
              labelText: 'Contraseña',
              hintText: 'Mínimo 6 caracteres',
              icon: Icon(
                Icons.lock,
                color: Theme.of(context).primaryColor,
              )),
        ));
  }

  Widget _formActions() {
    return Padding(
      padding: EdgeInsets.only(top: 20.0),
      child: Column(
        children: [
          _isSubmitting == true
              ? CircularProgressIndicator(
                  valueColor:
                      AlwaysStoppedAnimation(Theme.of(context).accentColor),
                )
              : RaisedButton(
                  child: Text(
                    'Iniciar sesión',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(color: Colors.black),
                  ),
                  elevation: 4.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  color: Theme.of(context).accentColor,
                  onPressed: _submit,
                ),
          FlatButton(
            child: Text('¿No tiene una cuenta? Regístrate aquí'),
            onPressed: () =>
                Navigator.pushReplacementNamed(context, '/register'),
          )
        ],
      ),
    );
  }

  void _submit() {
    final form = _formKey.currentState;

    if (form.validate()) {
      form.save();
      _registerUser();
    }
  }

  void _registerUser() async {
    setState(() => _isSubmitting = true);

    http.Response response = await http.post('http://10.0.2.2:1337/auth/local',
        body: {"identifier": _email, "password": _password});
    final responseData = json.decode(response.body);
    print(responseData);
    if (response.statusCode == 200) {
      setState(() => _isSubmitting = false);
      _storeUserData(responseData);
      _showSnack();
      _redirectUser();
    } else {
      setState(() => _isSubmitting = false);
      final String errorMsg =
          responseData['message'][0]['messages'][0]['message'];

      _showErrorSnack(errorMsg);
    }
  }

  void _storeUserData(responseData) async {
    final prefs = await SharedPreferences.getInstance();
    Map<String, dynamic> user = responseData['user'];
    user.putIfAbsent('jwt', () => responseData['jwt']);
    prefs.setString('user', json.encode(user));
  }

  void _showSnack() {
    final snackbar = SnackBar(
      content:
          Text('Te damos la bienvenida', style: TextStyle(color: Colors.green)),
      backgroundColor: Colors.transparent,
    );

    _scaffoldKey.currentState.showSnackBar(snackbar);
    _formKey.currentState.reset();
  }

  void _showErrorSnack(String errorMsg) {
    final snackbar = SnackBar(
      content: Text(errorMsg, style: TextStyle(color: Colors.red)),
      backgroundColor: Colors.transparent,
    );

    _scaffoldKey.currentState.showSnackBar(snackbar);

    throw Exception('Error al iniciar sesión: $errorMsg');
  }

  void _redirectUser() {
    Future.delayed(Duration(seconds: 2), () {
      Navigator.pushReplacementNamed(context, '/');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text('Inicio de sesión'),
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 30.0),
          child: Center(
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    _title(),
                    _emailInput(),
                    _passwordInput(),
                    _formActions()
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
